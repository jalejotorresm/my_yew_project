mod app;
mod components;
mod router;
mod styles;

use crate::app::App;

fn main() {
    yew::Renderer::<App>::new().render();
}
