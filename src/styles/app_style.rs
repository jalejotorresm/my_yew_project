use stylist::{style, Style};

#[allow(non_upper_case_globals)]
pub fn estilo_main() -> Style {
    let estilo = style!(
        r#"
    * {
      font-size: 40px;
      font-family: 'Quicksand';
      margin: 10px;
    }
    
    h1 {
      color: #93ff00;
    }
    
    p {
      color: #FF00FF;
      font-family: 'Poppins';
    }
    "#
    )
    .unwrap();

    estilo
}

#[allow(non_upper_case_globals)]
pub fn estilo_lista() -> Style {
    let estilo = style!(
        r#"
    * {
        font-family: 'Roboto';
    }

    .normal {
        color: green;    
    }

    .importante {
        color: yellow;
    }

    .urgente {
        color: red;
    }
    "#
    )
    .unwrap();

    estilo
}

#[allow(non_upper_case_globals)]
pub fn estilo_title() -> Style {
    let estilo = style!(
        r#"
    h1 {
        font-family: 'Poppins';
        color: #9EDC00;
    }
    "#
    )
    .unwrap();

    estilo
}

#[allow(non_upper_case_globals)]
pub fn estilo_nav() -> Style {
    let estilo = style!(
        r#"
  .menu {
    display: flex;
    flex-direction: row;
    justify-content: center;
  }

  a {
    color: antiquewhite;
    text-decoration: none;
  }

  a:hover {
    color:pink;
  }

  "#
    )
    .unwrap();

    estilo
}
