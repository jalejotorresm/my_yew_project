use yew::prelude::*;
use yew_router::prelude::*;

use crate::components::atoms::main_title::MainTitle;
use crate::components::molecules::{reuniones::Reuniones, tareas::Tareas};

#[derive(Clone, Routable, PartialEq)]
pub enum Route {
    #[at("/")]
    Home,
    #[at("/tareas")]
    Tareas,
    #[at("/reuniones")]
    Reuniones,
    #[not_found]
    #[at("/404")]
    NotFound,
}

pub fn switch(routes: Route) -> Html {
    match routes {
        Route::Home => html! { <MainTitle nombre="Alejandro" /> },
        Route::Tareas => html! { <Tareas /> },
        Route::Reuniones => html! { <Reuniones /> },
        Route::NotFound => html! { <h1>{ "404" }</h1> },
    }
}
