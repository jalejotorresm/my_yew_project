use yew::prelude::*;
use yew_router::prelude::*;

use crate::components::atoms::navbar::NavBar;
use crate::router::rutas::{switch, Route};
use crate::styles::app_style::estilo_main;

#[function_component(App)]
pub fn app() -> Html {
    html! {
        <BrowserRouter>
            <div class={estilo_main()}>
                <h1>{ "Bienvenido a mi pagina hecha con Yew y Rust" }</h1>
                <NavBar />
                <Switch<Route> render={switch} />
            </div>
        </BrowserRouter>
    }
}
