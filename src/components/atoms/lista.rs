use yew::prelude::*;

use crate::styles::app_style::estilo_lista;


#[derive(Properties, PartialEq)]
pub struct Props {
    pub lista: Vec<&'static str>,
    pub color: Color,
}

#[derive(PartialEq)]
#[allow(dead_code)]
pub enum Color {
    Normal,
    Importante,
    Urgente,
}

impl Color {
    pub fn prioridad (&self) -> String {
        match self {
            Color::Normal => "normal".to_owned(),
            Color::Importante => "importante".to_owned(),
            Color::Urgente => "urgente".to_owned(),
        }
    }
}

fn lista_a_html(lista: &Vec<&'static str>) -> Vec<Html> {
    lista
        .iter()
        .map(|tarea| {
            html! {
                <li key={lista.iter().position(|i| i == tarea).unwrap_or_default()}>{ tarea }</li>
            }
        })
        .collect()
}

#[function_component(Lista)]
pub fn lista(props: &Props) -> Html {
    html! {
        <div class={estilo_lista()}>
            <ul class={&props.color.prioridad()}>{ lista_a_html(&props.lista) }</ul>
        </div>
    }
}
