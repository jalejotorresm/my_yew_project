use yew::prelude::*;
use yew_router::prelude::*;

use crate::router::rutas::Route;
use crate::styles::app_style::estilo_nav;

#[function_component(NavBar)]
pub fn navbar() -> Html {
    html! {
        <div class={estilo_nav()}>
            <section class="menu">
                <Link<Route> classes="option" to={Route::Home}>{ "Inicio" }</Link<Route>>
                <Link<Route> classes="option" to={Route::Tareas}>{ "Tareas" }</Link<Route>>
                <Link<Route> classes="option" to={Route::Reuniones}>{ "Reuniones" }</Link<Route>>
            </section>
        </div>
    }
}
