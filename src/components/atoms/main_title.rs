use crate::styles::app_style::estilo_title;
use yew::prelude::*;

#[derive(Properties, PartialEq)]
pub struct Props {
    pub nombre: String,
}

#[function_component(MainTitle)]
pub fn main_title(props: &Props) -> Html {
    html! {
        <div class={estilo_title()}>
            <h1>{ format!("Hola, {}!!!", &props.nombre) }</h1>
        </div>
    }
}
