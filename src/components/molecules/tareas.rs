use yew::prelude::*;

use crate::components::atoms::lista::{Color, Lista};

#[function_component(Tareas)]
pub fn tareas() -> Html {
    let tareas = vec!["aprender Rust", "intentar descansar", "practicar codigo"];

    html! {
        <section>
            <p>{ "Tienes las siguientes tareas pendientes:" }</p>
            <Lista lista={tareas} color={Color::Urgente} />
        </section>
    }
}
