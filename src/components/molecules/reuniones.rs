use yew::prelude::*;

use crate::components::atoms::lista::{Color, Lista};

#[function_component(Reuniones)]
pub fn reuniones() -> Html {
    let personas = vec!["Camilo", "Carolina", "Luz"];

    html! {
        <section>
            <p>{ "Tienes las siguientes reuniones pendientes:" }</p>
            <Lista lista={personas} color={Color::Normal} />
        </section>
    }
}
